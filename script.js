"use strict"

const getFactorial = () => {
  let number = prompt("Введіть число:");

  while (!Number.isInteger(Number(number))) {
    number = prompt("Введіть коректне число:");
  }
  let factorial = 1;

  for (let i = 2; i <= number; i++) {
    factorial *= i;
  }

  alert(`Факторіал числа ${number} = ${factorial}.`);
};
getFactorial();
